import {Injectable} from '@angular/core';
import {environment} from '../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {tap} from 'rxjs/operators';

@Injectable()
export class ApiService {
  private basePath = environment.baseUrl;

  constructor(private httpClient: HttpClient) {
  }

  getDocument(): Observable<any> {
    return this.httpClient.get<any>(this.basePath + 'get-document',)
      .pipe(
        tap(res => console.log(res))
      );
  }

  updateStatus(event): Observable<any> {
    const data = new FormData();
    data.append('event', event);
    return this.httpClient.post<any>(this.basePath + '/update-status', data)
      .pipe(
        tap(res => console.log(res))
      );
  }
}

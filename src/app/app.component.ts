import {Component, OnInit} from '@angular/core';
import {ApiService} from './services/api-service';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'docusign-app';
  public loading = false;

  constructor(private apiService: ApiService, private route: ActivatedRoute) {
  }


  onClick() {
    this.loading = true;
    this.apiService.getDocument()
      .subscribe(res => {
        this.loading = false;
        window.location.href = res.url;
      }, err => {
        this.loading = false;
        console.log(err);
      });
  }

  ngOnInit(): void {
    console.log(this.route.queryParamMap);
    this.route.queryParamMap.subscribe(params => {
      console.log(params)
      const event: string = params.get('event');
      console.log(event);
      if (event) {

        this.apiService.updateStatus(event)
          .subscribe(res => {
              console.log(res);
            },
            err => {
              console.log(err);
            }
          );
      }
    });


  }
}
